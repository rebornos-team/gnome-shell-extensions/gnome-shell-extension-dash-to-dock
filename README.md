# gnome-shell-extension-dash-to-dock

Move the dash out of the overview transforming it in a dock

https://github.com/micheleg/dash-to-dock/

https://micheleg.github.io/dash-to-dock/

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/gnome-shell-extensions/gnome-shell-extension-dash-to-dock.git
```
